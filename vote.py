from PyQt5.QtWidgets import QWidget, QApplication, QRadioButton, QVBoxLayout, QGroupBox, QFileDialog, QDialog, \
    QLineEdit, QLabel, QPushButton, QMessageBox, QButtonGroup
import configparser
import sys
class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Abstimmer')
        def getfile():
            file, typ = QFileDialog.getOpenFileName(self, "Umfrage Auswählen", "test", "VoteFiles (*.vote)")
            if file == '':
                getfile()
            return file
        self.file = getfile()

        self.authentification = False
        self.usermode = False
        self.currentuser = ''
        self.config = configparser.ConfigParser()
        self.config.read(self.file)


        layout = QVBoxLayout()

        vote = QPushButton('Ok')
        vote.clicked.connect(self.savevote)


        self.buttonwidget, self.buttons = self.createradiobuttons(self.config['general']['question'],
                                                                  list(self.config['answers'].keys()))


        self.endvote()

        layout.addWidget(self.buttonwidget)
        layout.addWidget(vote)

        self.setLayout(layout)
        self.show()

    def endvote(self):#
        print(5)
        self.hide()
        self.authentification = False
        self.login(self.config)
        print(6)
        if not self.authentification:
            self.endvote()
        print(7)
        self.show()
        if self.buttons.checkedButton() is not None:
            #print('unchecked')
            self.buttons.checkedButton().setChecked(False)
        print(8)

    def savevote(self):
        print(self.usermode)
        print(1)
        if self.usermode:
            try:
                if self.currentuser in self.config['votedusers']:
                    print('name schon vorhanden')
                    n = int(self.config['answers'][self.config['votedusers'][self.currentuser]]) - 1
                    self.config['answers'][self.config['votedusers'][self.currentuser]] = str(n)

                    n2 = int(self.config['answers'][self.buttons.checkedButton().text()]) + 1

                    self.config['answers'][self.buttons.checkedButton().text()] = str(n2)
                    self.config['votedusers'][self.currentuser] = str(self.buttons.checkedButton().text())
                else:
                    print('name noch nicht vorhanden')
                    self.config['votedusers'][self.currentuser] = self.buttons.checkedButton().text()

                    n = int(self.config['answers'][self.buttons.checkedButton().text()]) + 1
                    self.config['answers'][self.buttons.checkedButton().text()] = str(n)


            except KeyError:
                self.config['votedusers'] = {self.currentuser: self.buttons.checkedButton().text()}
                self.config['answers'][self.buttons.checkedButton().text()] = '1'

        else:
            n = int(self.config['answers'][self.buttons.checkedButton().text()]) + 1
            print(2)

            self.config['answers'][self.buttons.checkedButton().text()] = str(n)

        with open(self.file, 'w') as f:
            self.config.write(f)
            print(3)
        self.config.read(self.file)
        print(4)
        self.endvote()

    def login(self, config):
        try:
            config['users']
            self.usermode = True
            dialog = QDialog()

            QLabel('Benutzername:', dialog).move(10, 10)
            username = QLineEdit(dialog)
            username.move(120, 6)
            QLabel('Passwort:', dialog).move(10, 50)
            password = QLineEdit(dialog)
            password.setEchoMode(QLineEdit.Password)
            password.move(120, 50)
            self.usermode = True

            def ok():
                try:
                    if config['users'][username.text()] == password.text():

                        self.authentification = True
                        self.currentuser = username.text()
                        dialog.close()
                    else:
                        msg = QMessageBox()
                        msg.setText('Das Passwort ist Falsch')
                        msg.setStandardButtons(QMessageBox.Retry)
                        msg.exec_()
                        self.authentification = False
                except KeyError:
                    msg = QMessageBox()
                    msg.setText('Der Benutzername ist Falsch')
                    msg.setStandardButtons(QMessageBox.Retry)
                    msg.exec_()
                    self.authentification = False

            test = QPushButton('Ok',dialog)
            test.pressed.connect(ok)
            test.move(10, 100)
            dialog.exec_()

        except KeyError:
            self.authentification = True
            self.usermode = False

    def createradiobuttons(self, question, answers):
        layout = QVBoxLayout()
        group = QGroupBox(question)
        button_group = QButtonGroup()
        buttons = []
        count = 0
        for x in answers:
            buttons.append(QRadioButton(x))
            layout.addWidget(buttons[count])
            button_group.addButton(buttons[count])
            count += 1
        group.setLayout(layout)
        return group, button_group


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())