from PyQt5.QtWidgets import QWidget, QApplication, QFileDialog, QVBoxLayout
import sys
import matplotlib.pyplot as plt
import configparser


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Umfragen Ergebnis')
        layout = QVBoxLayout()

        def getfile():
            file, typ = QFileDialog.getOpenFileName(self, 'Umfrage Auswählen', 'asdfasdfasdf',
                                                    'VoteFiles (*.vote)')
            if file == '':
                getfile()
            return file

        file = getfile()
        config = configparser.ConfigParser()
        config.read(file)

        try:
            config['user']
            self.usermode = True
        except KeyError:
            self.usermode = False

        if self.usermode:
            pass
        else:
            try:
                self.values = []
                for x in config['answers'].values():
                    if x == '':
                        self.values.append(0)
                    else:
                        self.values.append(int(x))
                a = plt.bar(config['answers'].keys(), self.values)
                plt.show()
            except KeyError:
                pass





if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_)
